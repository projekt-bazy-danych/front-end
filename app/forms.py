from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired, EqualTo, ValidationError
from app.models import Pyriteusers

class LoginForm(FlaskForm):
    login = StringField('Login', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Sign In')

class RegistrationForm(FlaskForm):
    login = StringField('Login', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField('Repeat password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Sign up')

    def validate_login(self, login):
        user = Pyriteusers.query.filter_by(login=login.data).first()
        if user is not None:
            print("User already in database.")
            raise ValidationError('This login is already taken.')
  
    def validate_password(self, password):
        if len(password.data) < 8:
            print("Password too short.")
            raise ValidationError('Password must be at least 8 characters long.')



class ChangePasswordForm(FlaskForm):
    newPassword = PasswordField('New password', validators=[DataRequired()])
    newPassword2 = PasswordField('Repeat new password', validators=[DataRequired(), EqualTo('newPassword')])
    submit = SubmitField('Update')

    def validate_password(self, newPassword):
        if len(newPassword.data) < 8:
            print("Password too short.")
            raise ValidationError('Password must be at least 8 characters long.')
