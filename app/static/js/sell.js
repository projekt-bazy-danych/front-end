function popUp(iid) {
    sell = confirm("Do You want to sell that item?");
    if(sell){
	$.ajax({
	    type: "POST",
	    url: "/sell_item",
	    contentType: "application/json",
	    data: JSON.stringify({sellid: iid}),
	    dataType: "json",
	    success: function() {
		location.reload();
	    }
	});
    }
    
}
