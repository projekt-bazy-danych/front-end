from flask import render_template, url_for, redirect, request, jsonify, make_response
from app import app, db
from app.forms import LoginForm, RegistrationForm, ChangePasswordForm
from app.models import Pyriteusers, Owned, Items, Spins
from flask_login import current_user, login_user, logout_user, login_required
import random

@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html', title='Home')

@app.route('/sign_in', methods=['GET', 'POST'])
def sign_in():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = Pyriteusers.query.filter_by(login=form.login.data).first()
        if user is None or not user.check_password(form.password.data):
            return redirect(url_for('sign_in'))
        login_user(user)
        return redirect('/index')
    return render_template('sign_in.html', title='Sign in', form=form)

@app.route('/sign_up', methods=['GET', 'POST'])
def sign_up():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = Pyriteusers(login=form.login.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('sign_in'))
    return render_template('sign_up.html', title='Sign up', form=form)

@app.route('/sign_out')
def sign_out():
    logout_user()
    return redirect(url_for('index'))

@app.route('/stash')
@login_required
def stash():
    owned_items = Owned.query.filter_by(userid=current_user.id).all()
    only_ids = [i.itemid for i in owned_items]
    owned_items_values = Items.query.filter(Items.id.in_(only_ids))
    svalues = []
    for o in only_ids:
        for oiv in owned_items_values:
            if o == oiv.id:
                svalues.append(oiv.value)

    ret_items = list(zip(owned_items, svalues))
    return render_template('stash.html', title='Stash', items=ret_items)
    

@app.route('/about')
def about():
    return render_template('about.html', title='About the game')

@app.route('/spin')
@login_required
def spin():
    all_items = Items.query.all()
    return render_template('spin.html', title='Spin', items=all_items)

@app.route('/sell_item', methods=['POST'])
def sell_item():
    data = request.get_json()
    user_item = Owned.query.filter_by(userid=current_user.id, itemid=data['sellid']).first()
    item_info = Items.query.filter_by(id=data['sellid']).first()
    if user_item.amount > 1: #only decrement amount
        user_item.amount -= 1
        current_user.credits += item_info.value
        db.session.commit()
    elif user_item.amount == 1: #delete row from Owned table
        user_item.amount -= 1
        current_user.credits += item_info.value
        Owned.query.filter_by(userid=current_user.id, itemid=data['sellid']).delete()
        db.session.commit()
	
    return jsonify(status="success")

@app.route('/get_mineral', methods=['POST'])
def get_mineral():
    data = request.get_json()
    groups = [0, 1, 2, 3]
    group_probabilites = [0.88, 0.08, 0.03, 0.01]
    if current_user.credits < 20:
        return "error"
    current_user.credits -= 20
    current_user.totalspin += 1
    rand_group = random.choices(groups, group_probabilites)[0]
    group_items = Items.query.filter_by(rarity=rand_group).all()
    group_items_ids = [i.id for i in group_items]
    rand_mineral = random.choices(group_items_ids)[0]
    exist_owned = Owned.query.filter_by(userid=current_user.id, itemid=rand_mineral).first()

    spin_record = Spins(userid=current_user.id, itemid=rand_mineral)
    db.session.add(spin_record)
    
    if exist_owned is not None:
        exist_owned.amount += 1
        db.session.commit()
    else:
        new_owned = Owned(userid=current_user.id, itemid=rand_mineral, amount=1)
        db.session.add(new_owned)
        db.session.commit()

    back_data = {"mineral": rand_mineral}
    return jsonify(status="success", data=back_data)


@app.route('/settings', methods=['GET', 'POST'])
@login_required
def settings():
    form = ChangePasswordForm()

    if form.validate_on_submit():
        current_user.set_password(form.newPassword.data)
        db.session.commit()
        #return redirect(url_for('sign_out'))
       
    return render_template('settings.html', title='Account settings', form=form)
