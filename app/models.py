from app import db, login
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin

class Pyriteusers(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(32))
    userpass = db.Column(db.String(128))
    credits = db.Column(db.Integer, default=100)
    totalspin = db.Column(db.Integer, default=0)

    def __repr__(self):
        return '<User {} with {} cr>'.format(self.login, self.credits)

    def set_password(self, password):
        self.userpass = generate_password_hash(password, 'sha256')

    def check_password(self, password):
        return check_password_hash(self.userpass, password)

class Items(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    rarity = db.Column(db.Integer)
    value = db.Column(db.Integer)

    def __repr__(self):
        return '<Item {}>'.format(self.name)

class Owned(db.Model):
    userid = db.Column(db.Integer)
    itemid = db.Column(db.Integer)
    amount = db.Column(db.Integer)
    id = db.Column(db.Integer, primary_key=True)

    def __repr__(self):
        return 'User {} has item {} in amount of {}'.format(self.userid, self.itemid, self.amount)

class Spins(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    userid = db.Column(db.Integer)
    itemid = db.Column(db.Integer)

    def __repr__(self):
        return 'Spin nr {} for user {} with win: {}'.format(self.id, self.userid, self.itemid)

@login.user_loader
def load_user(id):
    return Pyriteusers.query.get(int(id))
