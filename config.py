import os

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-always-guess'
    SQLALCHEMY_DATABASE_URI = 'oracle://dbp:dbdb123@104.41.231.142:1521/?service_name=XEPDB1'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
